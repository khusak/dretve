#include <stdio.h> 
#include <unistd.h>
#include <pthread.h> 

void *prikaziPoruku(void *poruka) {
  printf("Poruka je: %s\n", (char*) poruka);
  sleep(1);
}

int main(void) {
  pthread_t dPrikazi;

  printf("Prije dretvi\n");
  pthread_create(&dPrikazi, NULL, prikaziPoruku, "Saljem argument");

  pthread_join(dPrikazi, NULL);
  printf("Nakon dretvi\n");

  return 0;
}
