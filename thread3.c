#include <stdio.h> 
#include <unistd.h>
#include <pthread.h> 

int globalna;

void *prikazi() {
  printf("Globalna je: %d\n", globalna);
  sleep(1);
}

void *povecajGlobalnu() {
  globalna++;
}

int main(void) {
  pthread_t dPrikazi;
  pthread_t dPovecaj;

  printf("Prije dretvi\n");
  globalna = 10;
  pthread_create(&dPovecaj, NULL, povecajGlobalnu, NULL);
  pthread_create(&dPrikazi, NULL, prikazi, NULL);

  pthread_join(dPovecaj, NULL);
  pthread_join(dPrikazi, NULL);
  printf("Nakon dretvi\n");

  return 0;
}

/*

h00s@jane:/storage/stuff/dev/cdev/vub/thread$ ./a.out 
Prije dretvi
Globalna je: 11
Nakon dretvi
h00s@jane:/storage/stuff/dev/cdev/vub/thread$ ./a.out 
Prije dretvi
Globalna je: 10
Nakon dretvi

*/
