#include <stdio.h> 
#include <unistd.h>
#include <pthread.h> 

// smanjio sam broj dretvi na 10
#define DRETVI 10

int globalna;
pthread_mutex_t lokot;

void *povecaj(void *povecajZa) {
  int i;
  printf("Povecavam globalnu za: %d\n", *((int *) povecajZa));
  for (i = 0; i < 100000; i++);
  // cekam na semafor
  pthread_mutex_lock(&lokot);
  // kriticni odsjecak
  globalna = globalna + *((int *) povecajZa);
  // glupost, samo za dokaz semafora i kriticnog odsjecka
  sleep(1);
  // oslobadjam semafor
  pthread_mutex_unlock(&lokot);
  for (i = 0; i < 100000; i++);
}

int main(void) {
  pthread_t dPovecaj[DRETVI];
  const int povecajZa = 1;
  int i;

  globalna = 0;

  printf("Globalna je: %d\n", globalna);
  printf("Prije dretve\n");

  // inicijaliziram mutex
  pthread_mutex_init(&lokot, NULL);

  // stvaram dretve
  for (i = 0; i < DRETVI; i++) {
    pthread_create(&dPovecaj[i], NULL, povecaj, (void *) &povecajZa);
  }

  // cekam na zavrsetak svih dretvi
  for (i = 0; i < DRETVI; i++) {
    pthread_join(dPovecaj[i], NULL);
  }

  // unistavam mutex
  pthread_mutex_destroy(&lokot);

  printf("Nakon dretve\n");
  printf("Globalna je: %d\n", globalna);

  return 0;
}

/*
Nakon dretve
Globalna je: 10

real    0m10.003s
user    0m0.004s
sys     0m0.000s
*/