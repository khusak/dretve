#include <stdio.h> 
#include <unistd.h>
#include <pthread.h> 

void *vrtiDo(void *doBroja) {
  int i;
  for (i = 0; i < *((int*) doBroja); i++) {
    printf("%d ", i);
  }
  printf("\n");
}

int main(void) {
  pthread_t dPrikazi;
  int x;

  printf("Prije dretve\n");
  x = 10;
  pthread_create(&dPrikazi, NULL, vrtiDo, (void *) &x);

  pthread_join(dPrikazi, NULL);
  printf("Nakon dretve\n");

  return 0;
}
