#include <stdio.h> 
#include <unistd.h>
#include <pthread.h> 

void *dretva() {
  printf("Spavam sekundu\n");
  sleep(1);
}

int main(void) {
  pthread_t thread_id;

  printf("Prije dretve\n");
  pthread_create(&thread_id, NULL, dretva, NULL);
  pthread_join(thread_id, NULL);
  printf("Nakon dretve\n");
  
  return 0;
}
