#include <stdio.h> 
#include <unistd.h>
#include <pthread.h> 

void *radi(void *oznaka) {
  printf("Ja sam dretva %d\n", *((int *) oznaka));
  sleep(1);
}

int main(void) {
  pthread_t dRadi;
  int x;

  printf("Prije dretve\n");
  x = 1;
  pthread_create(&dRadi, NULL, radi, (void *) &x);
  pthread_join(dRadi, NULL);
  printf("Nakon dretve\n");

  return 0;
}
